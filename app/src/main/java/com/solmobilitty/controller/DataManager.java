package com.solmobilitty.controller;

import android.content.Context;
import android.net.ConnectivityManager;
import android.widget.Toast;

import com.solmobilitty.listeners.APIInterface;
import com.solmobilitty.listeners.OnResponseListener;
import com.solmobilitty.model.BackendData;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;

public class DataManager {

    private final APIInterface apiInterface;
    private final Context context;

    public DataManager(Context context) {
        this.context = context;
        apiInterface = APIClient.getClient().create(APIInterface.class);
    }

    public void loadData(String url, final OnResponseListener mListener) {
        if (!isNetworkConnected()){
            Toast.makeText(context, "Check ur internet connection!", Toast.LENGTH_LONG).show();
            return;
        }

        Call<List<BackendData>> call1 = apiInterface.getData(url);
        call1.enqueue(new Callback<List<BackendData>>() {
            @Override
            public void onResponse(Call<List<BackendData>> call, retrofit2.Response<List<BackendData>> response) {
                List<BackendData> resp = response.body();
                if (resp == null) {
                    // Toast.makeText(getApplicationContext(),getString(R.string.wrong_user),Toast.LENGTH_LONG).show();
                    return;
                }
                mListener.onFinish(resp);
            }

            @Override
            public void onFailure(Call<List<BackendData>> call, Throwable t) {
                Toast.makeText(context, "Error happened!", Toast.LENGTH_LONG).show();
                call.cancel();
            }
        });
    }

    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm.getActiveNetworkInfo() != null;
    }
}