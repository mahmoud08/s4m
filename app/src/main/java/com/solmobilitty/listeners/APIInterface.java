package com.solmobilitty.listeners;

import com.solmobilitty.model.BackendData;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Url;

public interface APIInterface {

    @GET
    Call<List<BackendData>> getData(@Url String url);
}
