package com.solmobilitty.listeners;

import com.solmobilitty.model.BackendData;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Url;

public interface OnResponseListener {
    void onFinish(List<BackendData> resp);
}
