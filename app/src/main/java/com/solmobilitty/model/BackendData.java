package com.solmobilitty.model;

import com.google.gson.annotations.SerializedName;

import java.util.Date;
import java.util.List;


public class BackendData {
    @SerializedName("ID")
    public Datum id;
    @SerializedName("NATIVE_DATE")
    public Datum NATIVE_DATE;
    @SerializedName("ANNOUNCEMENT_DATE")
    public Datum ANNOUNCEMENT_DATE;
    @SerializedName("EXPIRY")
    public Datum EXPIRY;
    @SerializedName("ANNOUNCEMENT_DESCRIPTION")
    public Datum ANNOUNCEMENT_DESCRIPTION;
    @SerializedName("ANNOUNCEMENT_TITLE")
    public Datum ANNOUNCEMENT_TITLE;
    @SerializedName("ANNOUNCEMENT_IMAGE")
    public Datum ANNOUNCEMENT_IMAGE;
    @SerializedName("ANNOUNCEMENT_IMAGE_THUMBNAIL")
    public Datum ANNOUNCEMENT_IMAGE_THUMBNAIL;
    @SerializedName("ANNOUNCEMENT_HTML")
    public Datum ANNOUNCEMENT_HTML;

    public class Datum {
        @SerializedName("Tag")
        public String Tag;
        @SerializedName("Value")
        public String Value;
        @SerializedName("TypeCode")
        public int TypeCode;
        @SerializedName("IsBinaryUnique")
        public boolean IsBinaryUnique;
    }
}
