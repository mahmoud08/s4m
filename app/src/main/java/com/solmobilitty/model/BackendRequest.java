package com.solmobilitty.model;

import com.google.gson.annotations.SerializedName;

public class BackendRequest {

    @SerializedName("Lang")
    public String lang;
    @SerializedName("Count")
    public int count;

    public BackendRequest(String lang, int count) {
        this.lang = lang;
        this.count = count;
    }
}
